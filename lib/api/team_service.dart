import 'dart:convert';
import 'package:http/http.dart' as http;

import '../model/consumption.dart';
import '../model/purchase.dart';

import 'api_config.dart';

class TeamService {

  static Future<List<Purchase>> fetchPurchases(groupId) async {
    final response =
    await http.get(Uri.parse('${ApiConfig.baseUrl}/team/$groupId/purchases'));

    if (response.statusCode == 200) {
      final jsonData = json.decode(response.body);
      final purchases = (jsonData as List).map((item) => Purchase(
        purchaseId: item["purchase_id"],
        buyer: item['username'],
        purchaseDate: DateTime.parse(item['purchase_date_time']),
        quantityBought: item['quantity_bought'],
        amount: double.parse(item['total_cost']),
        coffee_name: item['coffee_name'],
      )).toList();

      return purchases;
    } else {
      throw Exception('Failed to fetch purchases');
    }

  }

  static Future<List<Consumption>> fetchCoffeeDrinks(int teamId) async {
    final response =
    await http.get(Uri.parse('${ApiConfig.baseUrl}/team/$teamId/drinks'));

    if (response.statusCode == 200) {
      final jsonData = json.decode(response.body);
      final consumptions = (jsonData as List).map((item) => Consumption(
        consumptionId: item['consumption_id'],
        stockId: item['stock_id'],
        userId: item["user_id"],
        date: item['consumption_date_time'],
        quantity: item['quantity_consumed'],
      )).toList();
      return consumptions;
    } else {
      throw Exception('Failed to fetch coffee drinks');
    }

  }

  static Future<int?> addTeam(
      String name, String description, List<String> userList) async {
    final url = Uri.parse('${ApiConfig.baseUrl}/teams');
    try {
      final response = await http.post(
        url,
        headers: {'Content-Type': 'application/json'},
        body: json.encode({
          'name': name,
          'description': description,
          'userList': userList.join(','),
        }),
      );

      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        return responseData as int?;
      }
    } catch (e) {
      print('Error adding team: $e');
    }

    return null;

  }
}