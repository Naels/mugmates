import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mugmates/model/user_existance.dart';
import 'package:mugmates/model/user_password.dart';

import '../model/consumption.dart';
import '../model/purchase.dart';
import '../model/team.dart';

import 'package:crypto/crypto.dart';

import 'api_config.dart';

class UserService {

  static Future<List<Map<String, dynamic>>> fetchUsers(String excludedUserId) async {
    final url = Uri.parse('${ApiConfig.baseUrl}/users/exclude/$excludedUserId');

    try {
      final response = await http.get(url);

      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);

        return responseData.cast<Map<String, dynamic>>();
      }
    } catch (e) {
      print('Error fetching user data: $e');
    }

    return [];

  }

  static Future<List<Map<String, dynamic>>> getUsersByGroupId(int groupId) async {
    final url = Uri.parse('${ApiConfig.baseUrl}/users/$groupId');

    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final jsonData = jsonDecode(response.body);
        final List<Map<String, dynamic>> users =
          List<Map<String, dynamic>>.from(jsonData);
        return users;
      } else {
        throw Exception('Request failed with status: ${response.statusCode}');
      }
    } catch (e) {
      throw Exception('Error retrieving users: $e');
    }

  }

  static Future<UserPassword?> getPassword(String username) async {
    try {
      final response = await http.get(
          Uri.parse('${ApiConfig.baseUrl}/user/password/${username}'));

      if (response.statusCode == 200) {
        final jsonData = json.decode(response.body);
        if (jsonData != null) {
          return UserPassword(
            userId: jsonData['userId'],
            password: jsonData['password'],
          );
        } else {
          return null;
        }
      } else {
        throw Exception('Request failed with status: ${response.statusCode}');
      }
    } catch (e) {
      throw Exception('Error checking user existence: $e');
    }

  }

  static Future<UserExistence> addUser(
      String username, String email, String password) async {
    try {
      final url = Uri.parse('${ApiConfig.baseUrl}/user/username/exists');
      final response = await http.post(
        url,
        body: jsonEncode({
          'username': username,
          'email': email,
          'password': sha256.convert(utf8.encode(password)).toString(),
        }),
        headers: {'Content-Type': 'application/json'},
      );

      if (response.statusCode == 200) {
        final jsonData = jsonDecode(response.body);
        final exists = jsonData['exists'] as bool;
        final id = jsonData['userId'];

        if (exists) {
          final existingField = jsonData['existingField'];
          final message = existingField == 'username'
              ? 'Username already exists'
              : 'Email already exists';

          return UserExistence(
            exists: true,
            message: message,
            existingField: existingField,
          );
        } else {
          return UserExistence(
            exists: false,
            message: 'User inserted successfully',
            userId: id,
          );
        }
      } else {
        throw Exception('Request failed with status: ${response.statusCode}');
      }
    } catch (e) {
      throw Exception('Error adding user: $e');
    }

  }

  static Future<List<Team>> getUserData(userId) async {
    try {
      final response =
      await http.get(Uri.parse('${ApiConfig.baseUrl}/user/$userId'));

      if (response.statusCode == 200) {
        final jsonData = json.decode(response.body);
        final teamList = (jsonData['teams'] as List).map((team) => Team(
          groupId: team['group_id'],
          name: team['group_name'],
          description: team['description'],
        )).toList();
        return teamList;
      } else {
        print('Request failed with status: ${response.statusCode}');
        return [];
      }
    } catch (e) {
      print(e);
      return [];
    }

  }

  static Future<String?> getNameById(userId) async {
    try {
      final response =
      await http.get(Uri.parse('${ApiConfig.baseUrl}/user/usernameById/$userId'));
      if (response.statusCode == 200) {
        final jsonData = json.decode(response.body);
        return jsonData['username'].toString();
      } else {
        print('Request failed with status: ${response.statusCode}');
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }






  static Future<void> addCoffeeToStock(int groupId, int userId,
      String coffeeName, int quantity, double pricePerUnit) async {
    final url = Uri.parse('${ApiConfig.baseUrl}/coffee/addstock');

    try {
      final response = await http.post(
        url,
        body: json.encode({
          'groupId': groupId.toString(),
          'coffeeName': coffeeName,
          'quantity': quantity.toString(),
          'pricePerUnit': pricePerUnit.toString(),
          'userId': userId.toString(),
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      );
      if (response.statusCode == 200) {
        print('Coffee added to stock');
      } else {
        print('Failed to add coffee to stock');
      }
    } catch (e) {
      print('Error: $e');
    }

  }

  static Future<void> drinkCoffee(
      int groupId, int userId, int quantity) async {
    final url = Uri.parse('${ApiConfig.baseUrl}/coffee/drink');
    try {
      final response = await http.post(
        url,
        body: json.encode({
          'groupId': groupId.toString(),
          'userId': userId.toString(),
          'quantity': quantity,
        }),
        headers: {'Content-Type': 'application/json'},
      );
      if (response.statusCode == 200) {
        print('Coffee drink successful');
      } else {
        print('Coffee drink failed: ${response.body}');
      }
    } catch (e) {
      print('Error occurred while drinking coffee: $e');
    }
  }







  static Future<List<Purchase>> fetchPurchases(groupId) async {
    final response =
    await http.get(Uri.parse('${ApiConfig.baseUrl}/team/$groupId/purchases'));

    if (response.statusCode == 200) {
      final jsonData = json.decode(response.body);
      final purchases = (jsonData as List).map((item) => Purchase(
        purchaseId: item["purchase_id"],
        buyer: item['username'],
        purchaseDate: DateTime.parse(item['purchase_date_time']),
        quantityBought: item['quantity_bought'],
        amount: double.parse(item['total_cost']),
        coffee_name: item['coffee_name'],
      )).toList();

      return purchases;
    } else {
      throw Exception('Failed to fetch purchases');
    }

  }

  static Future<List<Consumption>> fetchCoffeeDrinks(int teamId) async {
    final response =
    await http.get(Uri.parse('${ApiConfig.baseUrl}/team/$teamId/drinks'));

    if (response.statusCode == 200) {
      final jsonData = json.decode(response.body);
      final consumptions = (jsonData as List).map((item) => Consumption(
        consumptionId: item['consumption_id'],
        stockId: item['stock_id'],
        userId: item["user_id"],
        date: item['consumption_date_time'],
        quantity: item['quantity_consumed'],
      )).toList();
      return consumptions;
    } else {
      throw Exception('Failed to fetch coffee drinks');
    }

  }

}