import 'dart:convert';
import 'package:http/http.dart' as http;

import '../model/consumption.dart';
import '../model/purchase.dart';




import 'api_config.dart';

class CoffeeService {

  static Future<bool> addCoffeeToStock(int groupId, int userId,
      String coffeeName, int quantity, double pricePerUnit) async {
    final url = Uri.parse('${ApiConfig.baseUrl}/coffee/addstock');

    try {
      final response = await http.post(
        url,
        body: json.encode({
          'groupId': groupId.toString(),
          'coffeeName': coffeeName,
          'quantity': quantity.toString(),
          'pricePerUnit': pricePerUnit.toString(),
          'userId': userId.toString(),
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      );
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print('Error: $e');
      return false;
    }
  }

  static Future<bool> drinkCoffee(
      int groupId, int userId, int quantity) async {
    final url = Uri.parse('${ApiConfig.baseUrl}/coffee/drink');
    try {
      final response = await http.post(
        url,
        body: json.encode({
          'groupId': groupId.toString(),
          'userId': userId.toString(),
          'quantityConsumed': quantity,
        }),
        headers: {'Content-Type': 'application/json'},
      );
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print('Error occurred while drinking coffee: $e');
      return false;
    }
  }







  static Future<List<Purchase>> fetchPurchases(groupId) async {
    final response =
    await http.get(Uri.parse('${ApiConfig.baseUrl}/team/$groupId/purchases'));

    if (response.statusCode == 200) {
      final jsonData = json.decode(response.body);
      final purchases = (jsonData as List).map((item) => Purchase(
        purchaseId:     item["purchase_id"],
        buyer:          item['buyer'],
        purchaseDate:   DateTime.parse(item['purchase_date_time']),

        quantityBought: item['quantity_bought'],
        amount:         item['total_cost'],
        coffee_name:    item['coffee_name'],
      )).toList();
      return purchases;
    } else {
      throw Exception('Failed to fetch purchases');
    }

  }

  static Future<List<Consumption>> fetchCoffeeDrinks(int teamId) async {
    final response =
    await http.get(Uri.parse('${ApiConfig.baseUrl}/team/$teamId/drinks'));

    if (response.statusCode == 200) {
      final jsonData = json.decode(response.body);
      final consumptions = (jsonData as List).map((item) => Consumption(
        consumptionId: item['consumption_id'],
        stockId: item['stock_id'],
        userId: item["user_id"],
        date: item['consumption_date_time'],
        quantity: item['quantity_consumed'],
      )).toList();
      return consumptions;
    } else {
      throw Exception('Failed to fetch coffee drinks');
    }

  }

}