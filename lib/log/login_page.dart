import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

import '../api/user_service.dart';
import '../screens/team_list.dart';

import 'package:crypto/crypto.dart';

class LoginPage extends StatefulWidget {
  final int selectedToggleIndex;

  const LoginPage({Key? key, required this.selectedToggleIndex}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with WidgetsBindingObserver {
  final TextEditingController usernameController  = TextEditingController();
  final TextEditingController passwordController  = TextEditingController();
  final TextEditingController emailController     = TextEditingController();

  String signinError = "none";

  late int userExistId;

  bool isKeyboardVisible    = false;
  bool isMissingCredentials = false;
  bool isPasswordCorrect    = true;
  bool isUserFound          = true;

  @override
  void initState() {
    super.initState();
    isPasswordCorrect = true;
    isUserFound = true;
    WidgetsBinding.instance?.addObserver(this);
    KeyboardVisibilityController().onChange.listen((bool visible) {
      setState(() {
        isKeyboardVisible = visible;
      });
    });

    usernameController.addListener(_onFieldChanged);
    passwordController.addListener(_onFieldChanged);
    emailController.addListener(_onFieldChanged);
  }

  @override
  void didUpdateWidget(LoginPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.selectedToggleIndex != oldWidget.selectedToggleIndex) {
      setState(() {
        isMissingCredentials = false;
        isPasswordCorrect = true;
        isUserFound = true;
        signinError = 'none';
      });
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void _onFieldChanged() {
    setState(() {
      isMissingCredentials = false;
      signinError = 'none';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: _buildPage(),
      ),
    );
  }

  Widget _buildPage() {
    return Padding(
      padding: const EdgeInsets.all(32.0),
      child: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: 50),
            Row(
              children: [
                Expanded(
                  child: _buildInputField("Username", usernameController),
                ),
                if (widget.selectedToggleIndex == 1) // Check if Sign In toggle is selected
                  const SizedBox(width: 8), // Add spacing between fields
                if (widget.selectedToggleIndex == 1) // Check if Sign In toggle is selected
                  Expanded(
                    child: _buildInputField("Email", emailController), // Add email input field
                  ),
              ],
            ),
            const SizedBox(height: 20),
            _buildInputField("Password", passwordController, isPassword: true),
            const SizedBox(height: 20),
            if (!isKeyboardVisible) _buildLoginButton(),
          ],
        ),
      ),
    );
  }

  Widget _buildInputField(String placeholder, TextEditingController controller, {bool isPassword = false}) {
    final border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(18),
      borderSide: BorderSide(color: isMissingCredentials ? Colors.red : Colors.white),
    );

    return TextField(
      style: TextStyle(color: isMissingCredentials ? Colors.red : Colors.white),
      controller: controller,
      decoration: InputDecoration(
        hintText: placeholder,
        hintStyle: TextStyle(color: isMissingCredentials ? Colors.red : Colors.white),
        enabledBorder: border,
        focusedBorder: border,
      ),
      obscureText: isPassword,
    );
  }

  Widget _buildLoginButton() {
    final buttonText = (widget.selectedToggleIndex == 0) ? "Login" : "Sign In";
    final isLogin = (widget.selectedToggleIndex == 0);

    return Column(
      children: [
        FractionallySizedBox(
          widthFactor: 1,
          child: ElevatedButton(
            onPressed: () async {
              setState(() {
                isMissingCredentials = usernameController.text.trim().isEmpty ||
                    passwordController.text.trim().isEmpty;
              });

              if (!isMissingCredentials) {
                if (isLogin) {
                  await _login();
                } else {
                  _signin();
                }
              }
            },
            style: ElevatedButton.styleFrom(
              primary: const Color.fromARGB(255, 134, 77, 47),
              shape: const StadiumBorder(),
              padding: const EdgeInsets.symmetric(vertical: 16),
            ),
            child: Text(
              buttonText,
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 20),
            ),
          ),
        ),
        if (!isPasswordCorrect)
          const Text(
            'Incorrect password',
            style: TextStyle(color: Colors.red),
          ),
        if (!isUserFound)
          const Text(
            'Unknown username',
            style: TextStyle(color: Colors.red),
          ),
        if (signinError != 'none')
          Text(
            '$signinError already taken',
            style: const TextStyle(color: Colors.red),
          ),
      ],
    );
  }

  Future<void> _login() async {
    final userPassword = await UserService.getPassword(usernameController.text);

    if (userPassword != null) {
      var sha256Result = sha256.convert(utf8.encode(passwordController.text));
      var hashedPassword = sha256Result.toString();

      if (hashedPassword == userPassword.password) {
        _goToNext(userPassword.userId);
      } else {
        setState(() {
          isPasswordCorrect = false;
        });
      }
    } else {
      setState(() {
        isUserFound = false;
      });
    }
  }

  void _signin() async {
    final user = await UserService.addUser(usernameController.text, emailController.text, passwordController.text);

    if(user.exists && user.userId == null){
      setState(() {
        signinError = user.existingField!;
      });
    }

    if (!user.exists && user.userId != null) _goToNext(user.userId?.toInt());
  }

  void _goToNext(userId) {
    setState(() {
      isPasswordCorrect = true;
      isUserFound = true;
    });
    usernameController.clear();
    passwordController.clear();
    emailController.clear();
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => TeamList(userExistId: userId)),
    );
  }
}