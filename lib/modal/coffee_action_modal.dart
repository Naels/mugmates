import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mugmates/api/coffee_service.dart';

import '../constants.dart';


class CoffeeActionModal extends StatefulWidget {
  final String action;
  final int groupId;
  final int userId;
  final VoidCallback? onStockAdded;
  final VoidCallback? onCoffeeDrink;

  const CoffeeActionModal({
    required this.action,
    required this.groupId,
    required this.userId,
    this.onStockAdded,
    this.onCoffeeDrink,
  });

  @override
  _CoffeeActionModalState createState() => _CoffeeActionModalState();
}

class _CoffeeActionModalState extends State<CoffeeActionModal> {

  int quantity = 1;
  double price = 0.0;
  bool _showQuantityError = false;

  void _incrementQuantity() {
    setState(() {
      quantity++;
    });
  }

  void _decrementQuantity() {
    if (quantity > 1) {
      setState(() {
        quantity--;
      });
    }
  }

  Future<bool> _addCoffeeToStock(String coffeeName, int quantity, double pricePerUnit) async {
    try {
      final result = await CoffeeService.addCoffeeToStock(
        widget.groupId,
        widget.userId,
        coffeeName,
        quantity,
        pricePerUnit,
      );
      return result;
    } catch (e) {
      print('Failed to add coffee to stock: $e');
      return false;
    }
  }

  Future<bool> _drinkCoffee(int quantityDrank) async {
    try {
      final result = await CoffeeService.drinkCoffee(
          widget.groupId,
          widget.userId,
          quantityDrank
      );
      return result;
    } catch(e) {
      print('Failed to drink coffee...');
      return false;
    }
  }

  void _goBack() {
    Navigator.pop(context);
  }

  void _errorStockLow() {
    if (mounted) {
      setState(() {
        _showQuantityError = true;
      });

      Timer(const Duration(seconds: 5), () {
        if (mounted) {
          setState(() {
            _showQuantityError = false;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    print(widget.groupId);
    return Container(
      height: 300,
      padding: const EdgeInsets.all(16),
      color: AppConstants.splashScreenColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Icon(
            Icons.coffee,
            size: 48,
            color: Colors.white,
          ),
          const SizedBox(height: 16),
          Text(
            widget.action == 'refill' ? 'Refill Coffee Stock' : 'Notify Coffee Drink',
            style: const TextStyle(fontSize: 24, color: Colors.white),
          ),
          const SizedBox(height: 16),
          if (widget.action == 'refill') ...[
            Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white), // Set border color to white
                      borderRadius: BorderRadius.circular(4), // Optional: Set border radius
                    ),
                    child: TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Quantity',
                        contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
                        labelStyle: TextStyle(color: Colors.white),
                        enabledBorder: InputBorder.none, // Hide the default border
                        focusedBorder: InputBorder.none, // Hide the border when focused
                      ),
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        setState(() {
                          quantity = int.parse(value);
                        });
                      },
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white), // Set border color to white
                      borderRadius: BorderRadius.circular(4), // Optional: Set border radius
                    ),
                    child: TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Price per Unit',
                        contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
                        labelStyle: TextStyle(color: Colors.white),
                        enabledBorder: InputBorder.none, // Hide the default border
                        focusedBorder: InputBorder.none, // Hide the border when focused
                      ),
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        setState(() {
                          price = double.parse(value);
                        });
                      },
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ]else ...[
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      onPressed: _decrementQuantity,
                      icon: const Icon(Icons.remove),
                        color: Colors.white
                    ),
                    Text(
                      '$quantity',
                      style: const TextStyle(fontSize: 24, color: Colors.white),
                    ),
                    IconButton(
                      onPressed: _incrementQuantity,
                      icon: const Icon(Icons.add),
                      color: Colors.white
                    ),
                  ],
                ),
                AnimatedOpacity(
                  duration: const Duration(milliseconds: 500),
                  opacity: _showQuantityError ? 1.0 : 0.0,
                  child: Text(
                    '$quantity is too high for the current stock',
                    style: const TextStyle(
                      fontSize: 16,
                      color: Colors.red,
                    ),
                  ),
                ),
              ],
            ),
          ],
          Container(
            width: MediaQuery.of(context).size.width * 0.5,
            margin: const EdgeInsets.only(top: 20.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: AppConstants.homeScreenColor, // Set the button color to homeScreenColor
              ),
              onPressed: () async {
                if (widget.action == 'refill') {
                  if(await _addCoffeeToStock('Espresso', quantity, price)){
                    widget.onStockAdded?.call();
                    _goBack();
                  }
                } else {
                  if(await _drinkCoffee(quantity)){
                    widget.onCoffeeDrink?.call();
                    _goBack();
                  } else {
                    _errorStockLow();
                  }
                }
              },
              child: Text(widget.action == 'refill' ? 'Refill stock' : "Let's drink!"),
            ),
          )
        ],
      ),
    );
  }
}