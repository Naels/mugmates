class Team {
  final int groupId;
  final String name;
  final String description;

  Team({required this.groupId, required this.name, required this.description});
}