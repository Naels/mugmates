class Purchase {
  final int purchaseId;
  final String buyer;
  final DateTime purchaseDate;
  final int quantityBought;
  final double amount;
  final String coffee_name;

  Purchase({
    required this.purchaseId,
    required this.buyer,
    required this.purchaseDate,
    required this.quantityBought,
    required this.amount,
    required this.coffee_name,
  });
}