class UserPassword {
  final int userId;
  final String password;

  UserPassword({required this.userId, required this.password});
}