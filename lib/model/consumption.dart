class Consumption {
  final int consumptionId;
  final int stockId;
  final int userId;
  final String date;
  final quantity;

  Consumption({
    required this.consumptionId,
    required this.stockId,
    required this.userId,
    required this.date,
    required this.quantity,
  });
}