class UserExistence {
  final bool exists;
  final String? existingField;
  final String? message;
  final int? userId;

  UserExistence({
    required this.exists,
    this.existingField,
    this.message,
    this.userId,
  });
}