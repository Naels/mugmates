import 'package:flutter/material.dart';

class CoffeeToggleButton extends StatefulWidget {
  final ValueChanged<int> onToggleChanged;

  const CoffeeToggleButton({Key? key, required this.onToggleChanged})
      : super(key: key);

  @override
  _CoffeeToggleButtonState createState() => _CoffeeToggleButtonState();
}

class _CoffeeToggleButtonState extends State<CoffeeToggleButton> {
  late List<bool> isSelected;

  @override
  void initState() {
    super.initState();
    isSelected = [true, false];
  }

  @override
  Widget build(BuildContext context) {
    const textStyleBold = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold,
    );

    const textStyleNormal =  TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.normal,
    );

    Widget buildChild(int index, String text) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Text(
          text,
          style: isSelected[index] ? textStyleBold : textStyleNormal,
        ),
      );
    }

    return ToggleButtons(
      borderRadius: BorderRadius.circular(10),
      selectedColor: Colors.white,
      fillColor: const Color.fromARGB(255, 134, 77, 47),
      onPressed: (int index) {
        setState(() {
          for (int i = 0; i < isSelected.length; i++) {
            isSelected[i] = i == index;
          }
        });
        widget.onToggleChanged(index);
      },
      isSelected: isSelected,
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      borderWidth: 2,
      borderColor: const Color.fromARGB(255, 134, 77, 47),
      children: [
        buildChild(0, 'Login'),
        buildChild(1, 'Sign In'),
      ],
    );
  }
}