import 'package:flutter/material.dart';
import 'package:path/path.dart';

class AppConstants {
  static const Color splashScreenColor          = Color.fromARGB(255, 46, 20, 8);
  static const Color homeScreenColor            = Color.fromARGB(255, 134, 77, 47);
  static const Color lightBackgroundColor       = Color.fromARGB(255, 173, 155, 136);
  static const Color lightDarkenBackgroundColor = Color.fromARGB(255, 169, 155, 143);
  static const Color cardColorText              = Color.fromARGB(255, 59, 59, 59);


  static const double fontSizeTextCard = 13;
}

