import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mugmates/api/team_service.dart';
import 'package:mugmates/api/user_service.dart';
import 'package:mugmates/constants.dart';
import 'package:mugmates/screens/team_list.dart';

class CreateTeamPage extends StatefulWidget {
  final int userId;

  CreateTeamPage({required this.userId});

  @override
  _CreateTeamPageState createState() => _CreateTeamPageState();
}

class _CreateTeamPageState extends State<CreateTeamPage> {
  final TextEditingController teamNameController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  List<Map<String, dynamic>> teams = [];
  List<String> selectedUsers = [];

  @override
  void initState() {
    super.initState();
    _getUsers();
  }

  @override
  void dispose() {
    teamNameController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  void _createTeam() async {
    final String teamName = teamNameController.text.trim();
    final String description = descriptionController.text.trim();

    if (teamName.isEmpty || description.isEmpty || selectedUsers.isEmpty) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text('Incomplete Information'),
          content: const Text('Please fill in all fields before trying to add  new team'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text(
                'OK',
                style: TextStyle(
                  color: AppConstants.splashScreenColor,
                ),
              ),
            ),
          ],
        ),
      );
      return;
    }
    else{
      if(!selectedUsers.contains(widget.userId.toString())) {
        selectedUsers.add(widget.userId.toString());
      }
      bool success = await _callAddTeam(teamName, description, selectedUsers);
      if (success) {
        _showSuccessDialog(teamName);
      }
    }
  }

  void _showSuccessDialog(String teamName) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Team Created'),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('$teamName created'),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>
                      TeamList(userExistId: widget.userId)),
                );
              },
              child: const Text(
                'OK',
                style: TextStyle(
                  color: AppConstants.splashScreenColor,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Future<bool> _callAddTeam(String name, String description, List<String> userList) async {
    if (name.isNotEmpty && description.isNotEmpty && userList.isNotEmpty) {
      final Future<int?> addTeamResult = TeamService.addTeam(name, description, userList);
      return addTeamResult.then((teamId) {
        if (teamId != null) {
          print('Team added. Team ID: $teamId');
          return true;
        } else {
          print('Error adding team');
          return false;
        }
      });
    }
    return false;
  }

  Future<void> _getUsers() async {
    try {
      final users = await UserService.fetchUsers(widget.userId.toString());
      setState(() {
        teams = users;
      });
    } catch (e) {
      print('Error fetching user data: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Create Team'),
        backgroundColor: AppConstants.lightBackgroundColor,
      ),
      body: Container(
        color: AppConstants.lightBackgroundColor,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextField(
                controller: teamNameController,
                decoration: const InputDecoration(
                  labelText: 'Team name',
                  labelStyle: TextStyle(color: AppConstants.homeScreenColor),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: AppConstants.homeScreenColor),
                  ),
                ),
              ),
              const SizedBox(height: 16),
              TextField(
                controller: descriptionController,
                decoration: const InputDecoration(
                  labelText: 'Description',
                  labelStyle: TextStyle(color: AppConstants.homeScreenColor),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: AppConstants.homeScreenColor),
                  ),
                ),
              ),

              const SizedBox(height: 16),

              Expanded(
                child: ListView.builder(
                  itemCount: teams.length,
                  itemBuilder: (context, index) {
                    final user = teams[index];

                    return Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: AppConstants.homeScreenColor,
                        textTheme: const TextTheme(
                          titleMedium: TextStyle(color: AppConstants.homeScreenColor),
                        ),
                      ),
                      child: CheckboxListTile(
                        title: Text(user['username']),
                        value: selectedUsers.contains(user['userId'].toString()),
                        activeColor: AppConstants.splashScreenColor,
                        onChanged: (checked) {
                          if (checked != null) {
                            setState(() {
                              if (checked) {
                                selectedUsers.add(user['userId'].toString());
                              } else {
                                selectedUsers.remove(user['userId'].toString());
                              }
                            });
                          }
                        },
                      ),
                    );
                  },
                ),
              ),

              const SizedBox(height: 16),
              ElevatedButton(
                onPressed: _createTeam,
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(AppConstants.splashScreenColor),
                ),
                child: const Text('Create Team'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}