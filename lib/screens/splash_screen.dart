import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mugmates/screens/home_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
    _navigateToHomeScreen();

  }

  void _navigateToHomeScreen() {
    Future.delayed(const Duration(seconds: 3), () {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (BuildContext context) => const HomeScreen()),
      );
    });
  }

  Widget _buildSplashScreen() {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 46, 20, 8),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/coffee_splash.png',
              color: Colors.white,
              fit: BoxFit.contain,
              height: 200,
              width: 200,
            ),
            const SizedBox(height: 20),
            const Text(
              'Mug Mates',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
            const SizedBox(height: 10),
            const Text(
              'Share, Sip, and Connect',
              style: TextStyle(
                fontSize: 16,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildSplashScreen();
  }
}