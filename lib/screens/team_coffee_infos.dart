import 'package:flutter/material.dart';
import 'package:mugmates/api/coffee_service.dart';
import 'package:mugmates/constants.dart';

import '../modal/coffee_action_modal.dart';
import '../model/consumption.dart';
import '../model/purchase.dart';
import '../api/user_service.dart';

class TeamCoffeeInfos extends StatefulWidget {
  final int groupId;
  final int userId;
  final String teamName;

  const TeamCoffeeInfos({Key? key,
    required this.groupId,
    required this.userId,
    required this.teamName
  })
      : super(key: key);

  @override
  _TeamCoffeeInfosState createState() => _TeamCoffeeInfosState();
}

class _TeamCoffeeInfosState extends State<TeamCoffeeInfos> {
  bool _showPurchases = true;
  List<Map<String, dynamic>> usersList = [];
  List<Purchase> purchasesList = [];
  List<Consumption> consumptionsList = [];

  void _fetchUsers() {
    UserService.getUsersByGroupId(widget.groupId).then((users) {
      setState(() {
        usersList = users;
      });
    }).catchError((error) {
      print('Error fetching users: $error');
    });
  }


  @override
  void initState() {
    super.initState();
    _fetchUsers();
    _refreshPurchases();
    _refreshConsumptions();
  }

  void _refreshPurchases() {
    CoffeeService.fetchPurchases(widget.groupId).then((purchases) {
      setState(() {
        purchasesList = purchases;
      });
    }).catchError((error) {
      print('Error fetching purchases: $error');
    });
  }

  void _refreshConsumptions() {
    CoffeeService.fetchCoffeeDrinks(widget.groupId).then((consumptions) {
      setState(() {
        consumptionsList = consumptions;
      });
    }).catchError((error) {
      print('Error fetching purchases: $error');
    });
  }

  void _showModal(BuildContext context, String action) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return CoffeeActionModal(
          action: action,
          groupId: widget.groupId,
          userId: widget.userId,
          onStockAdded: _refreshPurchases,
          onCoffeeDrink: _refreshConsumptions,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppConstants.lightBackgroundColor,
        title: Text('${widget.teamName}'),
        actions: [
          PopupMenuButton(
            itemBuilder: (context) => [
              PopupMenuItem(
                child: ListTile(
                  title: Text(
                    usersList.isNotEmpty
                        ? usersList.map((user) => user['username']).join(", ")
                        : 'No users found',
                    style: const TextStyle(color: Colors.black),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      body: Container(
        color: AppConstants.lightBackgroundColor,
        child: Column(
        children: [
          Container(
            height: 80,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  blurRadius: 10,
                  offset: const Offset(0, 3),
                ),
              ],
            ),
            padding: const EdgeInsets.all(16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _showPurchases = true;
                    });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: _showPurchases ? AppConstants.homeScreenColor : Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: _showPurchases
                          ? [
                        BoxShadow(
                          color: AppConstants.homeScreenColor.withOpacity(0.5),
                          blurRadius: 8,
                          offset: const Offset(0, 3),
                        ),
                      ]
                          : null,
                    ),
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: Text(
                      'Coffee Buy',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: _showPurchases ? FontWeight.bold : FontWeight.normal,
                        color: _showPurchases ? Colors.white : Colors.black,
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _showPurchases = false;
                    });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: _showPurchases ? Colors.white : AppConstants.homeScreenColor,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: _showPurchases
                          ? null
                          : [
                        BoxShadow(
                          color: AppConstants.homeScreenColor.withOpacity(0.5),
                          blurRadius: 8,
                          offset: const Offset(0, 3),
                        ),
                      ],
                    ),
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: Text(
                      'Coffee Drink',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: _showPurchases ? FontWeight.normal : FontWeight.bold,
                        color: _showPurchases ? Colors.black : Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: _showPurchases
                ? Stack (
              children: [
                ListView.builder(
                  itemCount: purchasesList.length,
                  itemBuilder: (context, index) {
                    final purchase = purchasesList[index];

                    return Card(
                      elevation: 20,
                      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                        //side: const BorderSide(color: AppColors.homeScreenColor)
                      ),
                      color: AppConstants.lightDarkenBackgroundColor,
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Stack(
                          children: [
                            Image.asset(
                              'images/espresso.png',
                              fit: BoxFit.contain,
                              height: 120,
                              width: 100,
                            ),
                            Positioned(
                              top: 0,
                              left: 110,
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      purchase.coffee_name,
                                      style: const TextStyle(
                                        fontSize: AppConstants.fontSizeTextCard,
                                        fontWeight: FontWeight.bold,
                                        color: AppConstants.cardColorText,
                                      ),
                                    ),
                                    const SizedBox(height: 6),
                                    Text(
                                      'Purchased By: ${purchase.buyer}',
                                      style: const TextStyle(
                                        fontSize: AppConstants.fontSizeTextCard,
                                        fontWeight: FontWeight.w200,
                                        color: AppConstants.cardColorText,
                                      ),
                                    ),
                                    const SizedBox(height: 6),
                                    Text(
                                      'Purchase Date: ${purchase.purchaseDate}',
                                      style: const TextStyle(
                                        fontSize: AppConstants.fontSizeTextCard,
                                        fontWeight: FontWeight.w200,
                                        color: AppConstants.cardColorText,
                                      ),
                                    ),
                                    const SizedBox(height: 6),
                                    Text(
                                      'Quantity Bought: ${purchase.quantityBought}',
                                      style: const TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w200,
                                        color: AppConstants.cardColorText,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              right: 0,
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                width: MediaQuery.of(context).size.width * 0.3,
                                decoration: const BoxDecoration(
                                  color: AppConstants.homeScreenColor,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    bottomRight: Radius.circular(20),
                                  ),
                                ),
                                child: Text(
                                  'Total : \$${purchase.amount.toStringAsFixed(2)}',
                                  style: const TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
                if (purchasesList.isEmpty)
                  const Center(
                    child: Text(
                      'New team, nothing to show yet !',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: AppConstants.homeScreenColor,
                      ),
                    ),
                  ),
              ],
            )
                :Stack(
              children: [
                ListView.builder(
                  itemCount: consumptionsList.length,
                  itemBuilder: (context, index) {
                    final consumption = consumptionsList[index];

                    return Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: Card(
                        elevation: 20,
                        margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        color: AppConstants.lightDarkenBackgroundColor,
                        child: Stack(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(16),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Icon(
                                    Icons.local_cafe,
                                    color: AppConstants.homeScreenColor,
                                    size: 36,
                                  ),
                                  const SizedBox(width: 16),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'User ID: ${consumption.userId.toString()}',
                                          style: const TextStyle(
                                            fontSize: AppConstants.fontSizeTextCard,
                                            fontWeight: FontWeight.bold,
                                            color: AppConstants.cardColorText,
                                          ),
                                        ),
                                        const SizedBox(height: 8),
                                        Text(
                                          'Drink Date: ${consumption.date}',
                                          style: const TextStyle(
                                            fontSize: AppConstants.fontSizeTextCard,
                                            fontWeight: FontWeight.w200,
                                            color: AppConstants.cardColorText,
                                          ),
                                        ),
                                        const SizedBox(height: 8),
                                        Text(
                                          'Quantity Drank: ${consumption.quantity}',
                                          style: const TextStyle(
                                            fontSize: AppConstants.fontSizeTextCard,
                                            fontWeight: FontWeight.w200,
                                            color: AppConstants.cardColorText,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              right: 0,
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                width: 150,
                                height: 27,
                                decoration: const BoxDecoration(
                                  color: AppConstants.homeScreenColor,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    bottomRight: Radius.circular(20),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
                if (consumptionsList.isEmpty)
                  const Center(
                    child: Text(
                      'New team, nothing to show yet !',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: AppConstants.homeScreenColor,
                      ),
                    ),
                  ),

              ],
            ),
          ),
          const SizedBox(height: 16),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () {
                  _showModal(context, 'refill');
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                ),
                child: const Column(
                  children: [
                    Icon(
                      Icons.coffee,
                      color: AppConstants.homeScreenColor,
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Refill Coffee Stock',
                      style: TextStyle(
                        color: AppConstants.cardColorText,
                      ),
                    ),
                  ],
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  _showModal(context, 'coffeeDrink');
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                ),
                child: const Column(
                  children: [
                    Icon(
                      Icons.local_cafe,
                      color: AppConstants.homeScreenColor,
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Notify Coffee Drink',
                      style: TextStyle(
                        color: AppConstants.cardColorText,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 16),
        ],
        ),
      ),
    );
  }
}