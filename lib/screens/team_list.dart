import 'package:flutter/material.dart';
import 'package:mugmates/api/user_service.dart';
import 'package:mugmates/screens/team_coffee_infos.dart';

import '../constants.dart';
import '../model/team.dart';
import 'create_team_page.dart';



class TeamList extends StatefulWidget {
  final int userExistId;

  TeamList({required this.userExistId});

  @override
  _TeamListState createState() => _TeamListState();
}

class _TeamListState extends State<TeamList> with WidgetsBindingObserver {
  List<Team> teams = [];
  String? usernameDB;

  @override
  void initState() {
    super.initState();
    _fetchUserData();
    WidgetsBinding.instance?.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  Future<void> _fetchUserData() async {
    try {
      final username = await UserService.getNameById(widget.userExistId);
      final userData = await UserService.getUserData(widget.userExistId);
      setState(() {
        usernameDB = username;
        teams = userData;
      });
    } catch (e) {
      print('Error fetching user data: $e');
    }
  }

  void _navigateToCreateTeamPage() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CreateTeamPage(userId: widget.userExistId),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppConstants.lightBackgroundColor,
      appBar: AppBar(
        title: Text(
          'Welcome ${usernameDB ?? 'back'}',
          style: TextStyle(color: AppConstants.splashScreenColor),
        ),
        backgroundColor: AppConstants.lightBackgroundColor,
      ),
      body: ListView.builder(
        itemCount: teams.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              ListTile(
                contentPadding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
                leading: Container(
                  width: 75,
                  height: 75,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: index % 2 == 0 ? Image.asset('images/baristo.png') : Image.asset('images/barista.png'),
                  ),
                ),
                title: Text(
                  teams[index].name,
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: AppConstants.splashScreenColor,
                  ),
                ),
                subtitle: Text(
                  teams[index].description,
                  style: const TextStyle(color: AppConstants.homeScreenColor),
                ),
                trailing: const Icon(Icons.arrow_forward),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => TeamCoffeeInfos(
                        groupId: teams[index].groupId,
                        userId: widget.userExistId,
                        teamName: teams[index].name
                      ),
                    ),
                  );
                },
              ),
              const Divider(),
            ],
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _navigateToCreateTeamPage,
        backgroundColor: AppConstants.splashScreenColor,
        child: const Icon(Icons.add,color: Colors.white,),
      ),
    );
  }
}