import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mugmates/components/coffee_toggle_btn.dart';
import 'package:mugmates/constants.dart';
import 'package:mugmates/log/login_page.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  int selectedToggleIndex = 0;
  bool showToggle = false;
  bool isKeyboardOpen = false;
  int keyBoardState = 1;

  @override
  void initState() {
    super.initState();
    _toggleDelay();
    WidgetsBinding.instance?.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  void _toggleDelay() {
    Future.delayed(const Duration(milliseconds: 1500), () {
      setState(() {
        showToggle = true;
      });
    });
  }

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    keyBoardState += 1;
    setState(() {
      isKeyboardOpen = keyBoardState % 2 == 0;
    });
  }

  Widget _buildCoffeeImage() {
    return Positioned(
      top: showToggle
          ? MediaQuery.of(context).size.height * 0.5 - 320
          : MediaQuery.of(context).size.height * 0.5 - 150,
      left: 0,
      right: 0,
      child: Center(
        child: Image.asset(
          'images/coffee_home.png',
          fit: BoxFit.contain,
          height: 300,
          width: 300,
        ),
      ),
    );
  }

  Widget _buildWelcomeText() {
    return Positioned(
      top: showToggle
          ? MediaQuery.of(context).size.height * 0.05
          : MediaQuery.of(context).size.height * 0.20,
      left: 0,
      right: 0,
      child: const FractionallySizedBox(
        widthFactor: 1.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Mug Mates',
              style: TextStyle(
                color: AppConstants.homeScreenColor,
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 8),
            Text(
              'Welcome',
              style: TextStyle(
                color: AppConstants.homeScreenColor,
                fontSize: 24,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLoginPage() {
    return Positioned(
      bottom: !isKeyboardOpen
          ? MediaQuery.of(context).size.height * 0.2
          : MediaQuery.of(context).size.height * 0 - 75,
      left: 0,
      right: 0,
      child: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.4,
          child: LoginPage(selectedToggleIndex: selectedToggleIndex),
        ),
      ),
    );
  }

  Widget _buildCoffeeToggleButton() {
    return Positioned(
      bottom: MediaQuery.of(context).size.height * 0.10,
      left: 0,
      right: 0,
      child: Center(
        child: CoffeeToggleButton(
          onToggleChanged: (index) {
            setState(() {
              selectedToggleIndex = index;
            });
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppConstants.lightBackgroundColor,
      body: Stack(
        children: [
          _buildCoffeeImage(),
          _buildWelcomeText(),
          if (showToggle) _buildLoginPage(),
          if (!isKeyboardOpen && showToggle) _buildCoffeeToggleButton(),
        ],
      ),
    );
  }

}